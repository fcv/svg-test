(function() {
	var CANVAS_ID = 'my-canvas';
	
	var $ = function(id) {
		return document.getElementById(id);
	} 
	
	var dom = function() {
		var resolveElement = function( e ) {
			if ( e != null) {
				if (e.constructor == String) {
					e = $(e);
				} else if (e.nodeType != 1) {
					// argument is not a Element... so do not return it..
					e = null;
				}
			}				
			return e;
		};
		return {
			/** 
			 * setAttributes
			 * @param {String|HTMLElement} target
			 * @param {String} eventName 
			 * @param {function} handler
			 * @return target element already resolved
			 */
			addEvent : function(target, eventName, func) {
				if (target == null) {
					throw new Error('\'target\' cannot be null');
				}
				var rtarget = resolveElement(target);
				if (rtarget != null) {
					target = rtarget;
				}
				
				if (target.addEventListener) {
					target.addEventListener( eventName, func, false );
				} else if (target.attachEvent) {
					var funcWithEvent = function() {
						func(window.event);						
					}
					target.attachEvent( 'on' + eventName, funcWithEvent );
				} else {
					throw new Error('target \'' + target + '\' does not provide \'addEventListener\' or \'attachEvent\'');
				}
				return target;
			},
			
			addEvents: function(target, events) {
				for (var x in events) {
					dom.addEvent(target, x, events[x]);	
				}
			}
		}
	}();
	
	window.onload = function() {
		var canvasH = 300;
		var canvasW = 500;
		
		var paper = Raphael($(CANVAS_ID), canvasW, canvasH);
		
		var top = 50;
		var left = 50;
		
		var leftCircle = paper.ellipse(left + 50, top, 10, 10); // draw a circle at coordinate 10,10 with radius of 10
		leftCircle.attr("fill", "blue");
		// c.node.onclick = function () { c.attr("fill", "red"); };
		
		leftCircle.node.onclick = function() {
			//alert('true')
			leftCircle.animate({ry: 0}, 200, function() {
				leftCircle.animate({ry: 10}, 200);	
			});
			
			//leftCircle.animate({height: 10}, 1000);
		}
				
		var rightCircle = paper.circle(left + 150, top, 10)
		rightCircle.attr("fill", "blue");
		
		var line = paper.path({stroke: "#000", 'stroke-width': 2})
				.moveTo(left, top)
				.qcurveTo(left, top + 100, left + 100, top + 100)
				.qcurveTo(left + 200, top + 100, left+200, top);
				
		var poing = paper.text(canvasW/2, canvasH/2, "Poing!");
		poing.attr({
			'font-size': '3em', 
			'font-weight': 'bold', 
			fill: 'red',
			opacity: 0
		});

		var showPoing = function() {
			poing.animate({opacity: 1}, 100, function() {
				poing.animate({opacity: 0}, 300);
			});
		}
		
		var currentX = left + 50;
		var currentY = top;
		var delta = 10;
		var poingSide = -1;
		dom.addEvent(document, 'keypress', function(e) {
			var code = e.keyCode;
			switch (code) {
				case 37: //-- left
					currentX -= delta;
					if (currentX <= 10) {// radio size
						currentX = 10;
						leftCircle.attr('cx', currentX)
						if (poingSide != 37) {
							poingSide = 37
							showPoing();
						}
					} else {
						poingSide = -1;
						leftCircle.attr('cx', currentX)	
					}
					break;
				case 38: //-- up
					currentY -= delta;
					if (currentY <= 10) {
						currentY = 10;
						leftCircle.attr('cy', currentY)
						if (poingSide != 38) {
							poingSide = 38;
							showPoing();
						}
					} else {
						poingSide = -1;
						leftCircle.attr('cy', currentY);
					}
					break;
				case 39: //-- right
					currentX += delta;
					if (currentX >= canvasW - 10) {
						currentX = canvasW - 10;
						leftCircle.attr('cx', currentX);
						if (poingSide != 39) {
							poingSide = 39;
							showPoing();	
						}
					} else {
						poingSide = -1;
						leftCircle.attr('cx', currentX);
					}
					
					break;
				case 40: //-- down
					currentY += delta;
					if (currentY >= canvasH - 10) { 
						currentY = canvasH - 10;
						leftCircle.attr('cy', currentY);
						if (poingSide != 40) {
							poingSide = 40;
							showPoing();
						}
					} else {
						poingSide = -1;
						leftCircle.attr('cy', currentY);
					}
					
					break;
				default:
					break;
			}
		})
					
	}
})()